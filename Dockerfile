#FROM tensorflow/tensorflow:latest-gpu-py3
#FROM pytorch/pytorch:latest
FROM pytorch/pytorch:1.3-cuda10.1-cudnn7-devel

## ensure locale is set during build
ENV LANG C.UTF-8

ARG DEBIAN_FRONTEND=noninteractive

RUN conda install -q -y pytorch torchvision nodejs Cython pandas scikit-learn matplotlib seaborn jupyter jupyterlab

RUN pip install keras && \
    pip install uproot && \
    pip install jupyter && \
    pip install jupyterhub && \
    pip install jupyterlab && \
    pip install matplotlib && \
    pip install torch torchvision && \
    pip install sklearn && \
    pip install tables && \
    pip install papermill pydot Pillow
    

RUN apt-get update && \
    apt-get install -y git debconf-utils curl wget && \
    echo "krb5-config krb5-config/add_servers_realm string CERN.CH" | debconf-set-selections && \
    echo "krb5-config krb5-config/default_realm string CERN.CH" | debconf-set-selections && \
    apt-get install -y krb5-user && \
    apt-get install -y vim less screen graphviz python3-tk wget && \
    apt-get install -y jq tree hdf5-tools bash-completion && \
    apt-get install -y nvidia-cuda-toolkit && \
    apt-get install -y gfortran

ARG NB_USER=jovyan
ARG NB_UID=1000
ENV USER ${NB_USER}
ENV HOME /home/${NB_USER}
ENV SHELL bash

RUN adduser --uid ${NB_UID} ${NB_USER} && passwd -d ${NB_USER}

WORKDIR ${HOME}
USER ${USER}
